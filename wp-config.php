<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpressuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'user_password_here' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[X`5ON>5f%y,A{|n!Nrdp-M9g&Vd:CT+LR1-w[&_j& j`s#0Gf|b8<H<`X0n?8+Q');
define('SECURE_AUTH_KEY',  ' p+*ooy7<C/+(Uh|v/6q4#xW!XAot9Mz|Ns}2r5_-2a(mT)Rb:]Ym>||L3MhuNCe');
define('LOGGED_IN_KEY',    'J]jiX^D$Wt`C ]r!JJrN}9)v@{fx$BTnx%i1WLuX~NNDo($#Cw:}K}_X~k]:~qo{');
define('NONCE_KEY',        ')*fQ?QSZhA5#x~g-O;FOka-v>I-8E0XRL;L;faaN5?nfU]<Q)7hOzWk>sfB-k@uZ');
define('AUTH_SALT',        '+^yu/jjudSy+!D(3Ab-YE/X%HY+b4bb<UZx=#n+d Af)6^Eph(v(~.8iC.F4_BTs');
define('SECURE_AUTH_SALT', '-ZU%<2ffK$Uog#c_<| B~gR8u|2*#JJuFp|ci|:V#2I:u{^LL`5h(zM6Cft_.GeZ');
define('LOGGED_IN_SALT',   '-Q|.@Qzej7`~rmP=}rWY*arwBvRhP+Dn(S{%`~Y(r!-d@}|vU.9Qm|QTV@c-L;B,');
define('NONCE_SALT',       'KjkFIw}M{j0(u;z$JMa&ZJvw>};o:fj+Tor7/u5+:IOb*@%(W@}iFZv}Nezx-O*|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
